﻿var app = angular.module('app');

app.controller("myProjectListCtrl", function ($scope, $http, $timeout) {

    $scope.OrderDetailData = [];
    $scope.MaterialsFromEpatecData = [];


    $scope.getValues= function () {

        $http.get("http://localhost:56702/app/testjson/OrderDetail.json").then(function (response) {
            $scope.OrderDetailData = response.data.project;
        });
        $http.get("http://localhost:56702/app/testjson/MaterialsFromEpatec.json").then(function (response2) {
            $scope.MaterialsFromEpatecData = response2.data.project;
        });

    }


});

